#include <iostream>

#define BLOCK_SIZE 1024
#define STEP 10

#define DEBUG 0

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line)
{
    if (code != cudaSuccess)
        std::cerr << "\"" << cudaGetErrorString(code) << "\" in file " << file << ":" << line << std::endl;
}

__global__
void Sum_shared(int n, int* input, int* result) {
    auto tid = blockDim.x * blockIdx.x + threadIdx.x;

    const auto cache_start = blockDim.x * blockIdx.x;
    const auto cache_end = cache_start + BLOCK_SIZE;

    if (tid > n)
        return;

    __shared__ int s_data[BLOCK_SIZE];
    s_data[threadIdx.x] = input[tid];
    __syncthreads();

    for (int i = 0; i < STEP && tid > i; ++i) {
        if (tid - i >= cache_start)
            result[tid] += s_data[tid - i - cache_start];
        else
            result[tid] += input[tid - i];
    }
    for (int i = 1; i < STEP && n - tid > i; ++i) {
        if (tid + i < cache_end)
            result[tid] += s_data[tid + i - cache_start];
        else
            result[tid] += input[tid + i];
    }
}


__global__
void Sum(int n, int* input, int* result) {
    auto tid = blockDim.x * blockIdx.x + threadIdx.x;

    if (tid > n)
        return;

    result[tid] = 0;

    for (int i = 0; i < STEP && tid > i; ++i)
        result[tid] += input[tid - i];
    for (int i = 1; i < STEP && n - tid > i; ++i)
        result[tid] += input[tid + i];
}


int main() {
    int N = 1 << 28;
    cudaSetDevice (3);

    int* h_array = new int[N];
    int* h_sum = new int[N];
    for (int i = 0; i < N; ++i) {
        h_array[i] = 1;
    }

    int* d_array;
    int* d_sum;
    unsigned int size = N * sizeof(int);
    cudaMalloc(&d_array, size);
    cudaMalloc(&d_sum, size);

    cudaMemcpy(d_array, h_array, size, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop, start_sh, stop_sh;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventCreate(&start_sh);
    cudaEventCreate(&stop_sh);

    int num_blocks = (N + BLOCK_SIZE - 1) / BLOCK_SIZE;

    // Запуск без разделяемой памяти

    cudaEventRecord(start);
    Sum<<<num_blocks, BLOCK_SIZE>>>(N, d_array, d_sum);
    cudaEventRecord(stop);
    cudaMemcpy(h_sum, d_sum, size, cudaMemcpyDeviceToHost);
    cudaEventSynchronize(stop);

    // Запуск с разделяемой памятью
    cudaEventRecord(start_sh);
    Sum_shared<<<num_blocks, BLOCK_SIZE>>>(N, d_array, d_sum);
#if DEBUG
    gpuErrchk( cudaPeekAtLastError() );
#endif
    cudaEventRecord(stop_sh);
#if DEBUG
    gpuErrchk( cudaMemcpy(h_sum, d_sum, size, cudaMemcpyDeviceToHost) );
#else
    cudaMemcpy(h_sum, d_sum, size, cudaMemcpyDeviceToHost);
#endif
    cudaEventSynchronize(stop_sh);

    float ms, ms_sh;
    cudaEventElapsedTime(&ms, start, stop);
    cudaEventElapsedTime(&ms_sh, start_sh, stop_sh);

    std::cout << ms << "ms / with shared: " << ms_sh << "ms" << std::endl;

    cudaEventDestroy(start);
    cudaEventDestroy(start_sh);
    cudaEventDestroy(stop);
    cudaEventDestroy(stop_sh);
    cudaFree(d_array);
    cudaFree(d_sum);
    delete[] h_array;
    delete[] h_sum;
}
